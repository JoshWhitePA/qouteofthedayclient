#!/bin/python
#Author:Joshua White
#Creation date: Oct 14, 2015
#Professor: Lisa Frye
#Class:CSC 328
#Due: October 26, 2015
#Using Python 2.6 interpreter on CentOS 6.6
#This program connects to a quote of the day server based on the RFC 865
#The hostname of the QOTD server should be passed as a command-line argument.
# The program also allows for a regular IP address to be passed as well

import socket
import sys

def main(argv):
    #Usage statement
    if len(sys.argv) == 1:
        print "Usage: python QOTD.py <IP Address> | <Domain Name>"
        sys.exit()
    for arg in sys.argv: 1
    QOTDIP = arg
    if QOTDIP != None:
        try:
            socket.inet_aton(QOTDIP)
            # legal, CMD line args are Octets
        except socket.error:
            addr = socket.gethostbyname(QOTDIP)
            # this is a hostname, resolve to IP
    elif QOTDIP == None:
        print '\n Error: Need command line agruments\nUsage: python QOTD.py <Hostname> | <IP Address>'

    QOTDPort = 17
    #sends message server so it knows to respond
    initMess="Howdy World"
    #Setup socket
    try:
    	srvsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        #set how long to wait for timeout and socket should be closed
        srvsock.settimeout(10)
        #Connect to the socket
        srvsock.connect((QOTDIP, QOTDPort))
        #send the initial message to socket to let it know you want a response
        srvsock.sendall(initMess)
        datum = " "
        data = ""

        while datum != "":
        	datum = srvsock.recv(512)#buffer size allowed in bytes
        	data += datum
    except socket.error as errmsg:
        print "Error in socket:" + str(errmsg)
    print data #print the output

    srvsock.close()#close the socket


if __name__ == "__main__":
   main(sys.argv[1:])
